# Project 7: Adding authentication and user interface to brevet time calculator service

Maintainer: Alex Vischer
email: avischer@uoregon.edu

To register a new user, curl a json object with the username and password to http://localhost:5000/api/register. You will be added, and from there you can optionally choose to request a token at http://localhost:5000/api/token. To use the token, you must attempt to access one of the resources located at http://locahost/<resouce> with curl, using the token as if it were a username (including a password, but the password can be anything). For example:

curl -u eyJhbGciOiJIUzI1NiIsImlhdCI6MTU0MzU2MTkzOSwiZXhwIjoxNTQzNTYyNTM5fQ.eyJpZCI6IjAifQ.ZrspB9NC4aBNLIS-JFPSMvYxStEYF5gnRrJZA_iOFxE:hello -i -X GET http://localhost:5000/listAll

will access the list of times. The apis can still be found at http://localhost:5000/<resource>

In order to access any of the material using a browser, first navigate to /api/register where you can register.