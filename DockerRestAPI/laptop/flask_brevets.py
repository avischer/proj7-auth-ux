"""
Replacement for RUSA ACP brevet time calculator
(see https://rusa.org/octime_acp.html)

"""

import flask
from flask import request, abort, url_for, g, redirect, session
import arrow  # Replacement for datetime, based on moment.js
import acp_times  # Brevet time calculations
import config
from pymongo import MongoClient
from flask_restful import Resource, Api, reqparse
from flask_login import (LoginManager, current_user, login_required,
                            login_user, logout_user, UserMixin, 
                            confirm_login, fresh_login_required)

import os
import logging
from functools import wraps
from users import *
from flask_httpauth import HTTPBasicAuth
from wtforms import Form, StringField, validators


###
# Globals
###

app = flask.Flask(__name__)
CONFIG = config.configuration()
app.secret_key = CONFIG.SECRET_KEY
client = MongoClient(os.environ['DB_PORT_27017_TCP_ADDR'], 27017)
db = client.calcdb
api = Api(app)
auth = HTTPBasicAuth()

###
# Decorators
###


def log_req(f):
    @wraps(f)
    def decorated(*args, **kwargs):
        if ('is_auth' in session and session['is_auth']):
            session['is_auth'] = False
            session['next_url'] = None
            return f(*args, **kwargs)
        if ('user' not in g or g.user is None) and request.method == "GET":
            session['next_url'] = request.full_path
            return flask.redirect('/api/register')
        return f(*args, **kwargs)
    return decorated

###
# Forms
###


class RegistrationForm(Form):
    username = StringField('Username', [validators.InputRequired()])
    password = StringField('Password', [validators.InputRequired()])


###
# Pages
###


@app.route("/times")
def times():
    time_table = {}
    for i, times in enumerate(db.times.find()):
        del times['_id']
        time_table["row_" + str(i)] = times
    return flask.render_template("times.html", times=time_table)



@app.route("/")
@app.route("/index")
@auth.login_required
def index():
    app.logger.debug("Main page entry")
    return flask.render_template('calc.html')


@app.errorhandler(404)
def page_not_found(error):
    app.logger.debug("Page not found")
    flask.session['linkback'] = flask.url_for("index")
    return flask.render_template('404.html'), 404

###############
#
# AJAX request handlers
#   These return JSON, rather than rendering pages.
#
###############


@app.route("/_submit")
def _submit():
    db.times.drop()
    get = request.args.get
    for i in range(1,int(get("length"))):
        db.times.insert_one({
            "miles": get("miles" + str(i)),  
            "km": get("km" + str(i)),
            "location": get("location" + str(i)),
            "open": get("open" + str(i)),
            "close": get("close" + str(i))
        })
    return flask.jsonify(result={})


@app.route("/_calc_times")
def _calc_times():
    """
    Calculates open/close times from miles, using rules
    described at https://rusa.org/octime_alg.html.
    Expects one URL-encoded argument, the number of miles.
    """
    app.logger.debug("Got a JSON request")
    km = request.args.get('km', 999, type=float)
    dist = request.args.get('dist', 200, type=float)
    time = request.args.get('time', "00:00", type=str)
    date = request.args.get('date', "2017-01-01", type=str)
    app.logger.debug("km={}".format(km))
    app.logger.debug("request.args: {}".format(request.args))
    arrow_time = arrow.get(date + " " + time, "YYYY-MM-DD HH:mm")
    open_time = acp_times.open_time(km, dist, arrow_time)
    close_time = acp_times.close_time(km, dist, arrow_time)
    result = {"open": open_time.isoformat(), "close": close_time.isoformat()}
    return flask.jsonify(result=result)

######
# APIs
######


def get_open_and_close_times():
    open_close_dict = {"open": [], "close": []}
    for time in db.times.find():
        if time["open"] == "":
            continue
        open_time = arrow.get(time["open"], "ddd M/D H:mm")
        close_time = arrow.get(time["close"], "ddd M/D H:mm")
        open_close_dict["open"].append(open_time)
        open_close_dict["close"].append(close_time)
    open_close_dict["open"].sort()
    open_close_dict["close"].sort()   
    open_close_dict["open"] = [time.format("ddd M/D H:mm") for time in open_close_dict["open"]]
    open_close_dict["close"] = [time.format("ddd M/D H:mm") for time in open_close_dict["close"]]
    return open_close_dict


class OpenAndClose(Resource):
    #@log_req
    @auth.login_required
    def get(self):
        return get_open_and_close_times()


class Open(Resource):
    #@log_req
    @auth.login_required
    def get(self):
        return get_open_and_close_times()["open"]


class Close(Resource):
    #@log_req
    @auth.login_required
    def get(self):
        return get_open_and_close_times()["close"]


class AllCSV(Resource):
    ##@log_req
    @auth.login_required
    def get(self):
        data = get_open_and_close_times()
        parser = reqparse.RequestParser()
        parser.add_argument("top")
        args = parser.parse_args()
        k = args["top"]
        k = k if k is not None else len(data["open"])
        csv = ""
        for index, op in enumerate(data["open"][:int(k)]):
            csv += op + "," + data["close"][index] + "\r\n"
        return flask.make_response(csv)


class OpenCSV(Resource):
    #@log_req
    @auth.login_required
    def get(self):
        data = get_open_and_close_times()["open"]
        parser = reqparse.RequestParser()
        parser.add_argument("top")
        args = parser.parse_args()
        k = args["top"]
        k = k if k is not None else len(data)
        csv = ""
        for op in data[:int(k)]:
            csv += op + "\r\n"
        return flask.make_response(csv)


class CloseCSV(Resource):
    #@log_req
    @auth.login_required
    def get(self):
        data = get_open_and_close_times()["close"]
        parser = reqparse.RequestParser()
        parser.add_argument("top")
        args = parser.parse_args()
        k = args["top"]
        k = k if k is not None else len(data)
        csv = ""
        for cl in data[:int(k)]:
            csv += cl + "\r\n"
        return flask.make_response(csv)


class AllJSON(Resource):
    #@log_req
    @auth.login_required
    def get(self):
        return flask.make_response(flask.jsonify(get_open_and_close_times()))


class OpenJSON(Resource):
    #@log_req
    @auth.login_required
    def get(self):
        data = get_open_and_close_times()
        parser = reqparse.RequestParser()
        parser.add_argument("top")
        args = parser.parse_args()
        k = args["top"]
        k = k if k is not None else len(data["open"])
        return flask.make_response(flask.jsonify(data["open"][:int(k)]))


class CloseJSON(Resource):
    #@log_req
    @auth.login_required
    def get(self):
        data = get_open_and_close_times()
        parser = reqparse.RequestParser()
        parser.add_argument("top")
        args = parser.parse_args()
        k = args["top"]
        k = k if k is not None else len(data["close"])
        return flask.make_response(flask.jsonify(data["close"][:int(k)]))


api.add_resource(CloseJSON, "/listCloseOnly/json")
api.add_resource(OpenJSON, "/listOpenOnly/json")
api.add_resource(AllJSON, "/listAll/json")
api.add_resource(CloseCSV, "/listCloseOnly/csv")
api.add_resource(OpenCSV, "/listOpenOnly/csv")
api.add_resource(AllCSV, "/listAll/csv")
api.add_resource(OpenAndClose, "/listAll")
api.add_resource(Open, "/listOpenOnly")
api.add_resource(Close, "/listCloseOnly")


@app.route('/api/users/<id>')
def get_user(id):
    user = User.query_by_id(u'{}'.format(id))
    return user


@app.route('/api/register', methods=['POST', 'GET'])
def register():
    if request.method == "POST":
        try:
            username = request.json.get('username')
            password = request.json.get('password')
        except:
            username = request.form.get('username')
            password = request.form.get('password')
        if username is None or password is None:
            abort(400)
        if User.query_by_username(username) is not None:
            abort(400)
        user = User(username, password)
        return flask.jsonify({'username': username}), 201, {'Location': '/api/users/{}'.format(user.id)}
    if request.method == "GET":
        return flask.render_template("register.html")


@app.route('/api/token', methods=['GET'])
@auth.login_required
def get_auth_token():
    token = g.user.generate_auth_token(CONFIG.SECRET_KEY)
    return flask.jsonify({'token': token.decode('ascii')})


@auth.verify_password
def verify_password(username, password):
    user = User.verify_auth_token(CONFIG.SECRET_KEY, username)
    if not user:
        user = User.query_by_username(username)
        if not user or not user.verify_password(password):
            return False
    g.user = user
    return True

#############



#############

app.debug = CONFIG.DEBUG
if app.debug:
    app.logger.setLevel(logging.DEBUG)

if __name__ == "__main__":
    print("Opening for global access on port {}".format(CONFIG.PORT))
    app.run(port=CONFIG.PORT, host="0.0.0.0", threaded=True)
