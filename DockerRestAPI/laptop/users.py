import password
from itsdangerous import (TimedJSONWebSignatureSerializer
                          as Serializer, BadSignature, SignatureExpired)


class User():
    current_id = 0
    users = []
    usernames = []

    def __init__(self, name, pw, active=True, auth=True, anon=False):
        self.is_authenticated = auth
        self.is_anonymous = anon
        self.id = u'{}'.format(User.current_id)
        self.username = name
        self.is_active = active
        self.password_hash = password.hash_password(pw)
        User.current_id += 1
        User.users.append(self)
        User.usernames.append(name)

    def get_id(self):
        return self.id

    def verify_password(self, pw):
        return password.verify_password(pw, self.password_hash)
    
    @staticmethod
    def query_by_username(username):
        for user in User.users:
            if user.username == username:
                return user
        return None
    
    @staticmethod
    def query_by_id(id):
        for user in User.users:
            if user.id == id:
                return user
        return None

    def generate_auth_token(self, key, expiration=600):
        s = Serializer(key, expires_in=expiration)
        return s.dumps({'id': self.id})
    
    @staticmethod
    def verify_auth_token(key, token):
        s = Serializer(key)
        try:
            data = s.loads(token)
        except SignatureExpired:
            return None
        except BadSignature:
            return None
        user = User.query_by_id(data['id'])
        return user

        